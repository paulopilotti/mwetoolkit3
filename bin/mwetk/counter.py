from mwetk.filetype.indexlib import index_escape
from mwetk.base.frequency import Frequency
from mwetk.filetype.indexlib import Index
from mwetk import util
from mwetk.index import build_entry_builder
import re


################################################################################

def append_counters_to_candidates(index, composite_rule, candidates, dataset_name, ctxinfo=None):
    """
        Iterates through candidates list and uses index for counting occurences of each candidate
    """
    build_entry = build_entry_builder(composite_rule)
    suffix_array = index.load(composite_rule, ctxinfo)
    get_freq_function = create_freq_index_func(suffix_array, build_entry)
    for candidate in candidates:
        append_counters(ctxinfo, candidate, get_freq_function, dataset_name)


################################################################################

def append_counters(ctxinfo, ngram, get_freq_function, freq_name, count_joint_frequency=True, count_bigrams=False):
    """
        Calls the frequency function for each word of the n-gram as well as for
        the n-gram as a whole. The result is appended to the frequency list
        of the `Ngram` given as input.

        If the option "--bigrams" is active, calls the frequency function for
        each bigram in the 'Ngram'.

        @param ngram The `Ngram` that is being counted.
    """
    (c_surfaces, c_lemmas, c_pos) = ([], [], [])

    # Counts each 1gram separatedly
    for w in ngram:
        c_surfaces.append(w.surface)
        c_lemmas.append(w.lemma)
        c_pos.append(w.pos)
        freq_value = get_freq_function(ctxinfo, [w.surface], [w.lemma], [w.pos])
        w.add_frequency(Frequency(freq_name, freq_value))

    # Global frequency (freq of the whole n-gram as a unit)
    if count_joint_frequency:
        freq_value = get_freq_function(ctxinfo, c_surfaces, c_lemmas, c_pos)
        ngram.add_frequency(Frequency(freq_name, freq_value))

    # Bigrams frequency
    if count_bigrams:
        i = 0
        while i < len(ngram) - 1:
            freq_value = get_freq_function(
                ctxinfo, 
                c_surfaces[i:i + 2],
                c_lemmas[i:i + 2],
                c_pos[i:i + 2]
            )
            i = i + 1
            ngram.add_bigram(Frequency(freq_name, freq_value))

################################################################################


def create_freq_index_func(suffix_array, build_entry):
    def get_freq_index(ctxinfo, surfaces, lemmas, pos):
        """
            Gets the frequency (number of occurrences) of a token (word) in the
            index file. Calling this function assumes that you called the script
            with the -i option and with a valid index file.

            @param surfaces A string corresponding to the surface form of a word.

            @param lemmas A string corresponding to the lemma of a word.

            @param pos A string corresponding to the Part Of Speech of a word.
        """
        ngram_ids = []
        # pdb.set_trace()
        for i in range(len(surfaces)):
            word = build_entry(index_escape(surfaces[i]),
                               index_escape(lemmas[i]),
                               index_escape(pos[i]))
            wordid = suffix_array.symbols.escaped_symbol_to_number.get(
                word, None)
            if wordid:
                ngram_ids.append(wordid)
            else:
                return 0

        indexrange = suffix_array.find_ngram_range(ngram_ids)
        if indexrange is not None:
            first, last = indexrange
            return last - first + 1
        else:
            return 0
    return get_freq_index


################################################################################

def load_index(ctxinfo, prefix):
    """
    Open the index files (valid index created by the `index.py` script). 
    @param prefix The string name of the index file.
    """

    if not prefix.endswith(".info"):
        ctxinfo.error("Index filename should have extension .info")
    prefix = prefix[:-len(".info")]

    util.verbose("Loading index files... this may take some time.")
    index = Index(prefix, ctxinfo=ctxinfo)
    index.load_metadata(ctxinfo)
    freq_name = re.sub(".*/", "", prefix)
    corpus_size = index.metadata["corpus_size"]
    return freq_name, corpus_size, index
