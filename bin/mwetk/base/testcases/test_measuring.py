#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

from ..measuring import OneSidedComparison
from ..measuring import EvaluationResult


def test_ose_add():
    ose = OneSidedComparison()
    ose.add(1, 3)
    assert ose == OneSidedComparison((1, 3))

    ose.add(1, 2)
    assert ose == OneSidedComparison((2, 5))

def test_se_evaluate_float():
    ose = OneSidedComparison()
    ose.add(1, 3)
    ose.add(1, 2)
    assert ose.evaluate_float() == 0.4  # 2/5
    
def test_er__init__():
    er = EvaluationResult()
    assert er == EvaluationResult(((0, 0), (0, 0)))

def test_er_add():
    er = EvaluationResult()
    er.prediction_comparison.add(1, 3)
    assert er == EvaluationResult(((1, 3), (0, 0)))
    er.prediction_comparison.add(1, 2)
    assert er == EvaluationResult(((2, 5), (0, 0)))

def test_er_precision():
    er = EvaluationResult()
    er.prediction_comparison.add(1, 3)
    er.prediction_comparison.add(1, 2)
    assert er.precision() == 0.4

def test_er_recall():
    er = EvaluationResult()
    er.prediction_comparison.add(1, 3)
    er.prediction_comparison.add(1, 2)
    er.reference_comparison.add(4, 8)
    assert er == EvaluationResult(((2, 5), (4, 8)))
    assert er.recall() == 0.5

def test_er_mult():
    er = EvaluationResult()
    er.prediction_comparison.add(1, 3)
    er.prediction_comparison.add(1, 2)
    er.reference_comparison.add(4, 8)
    er = 2*er
    assert er == EvaluationResult(((4, 10), (8, 16)))
    assert (er + EvaluationResult(((0, 0), (1, 1))) ) == EvaluationResult(((4, 10), (9, 17)))
    
