#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

import operator

from ..sentence import Sentence
from ..sentence import SentenceFactory
from ..ngram import Ngram
from ..word import Word

from ..feature import FeatureSet

from ..candidate import CandidateFactory
from ..candidate import Candidate


def SentenceWordlist1():
    return [Word({"surface":"J'","lemma":"je","pos":"pronoun"}), Word({"surface":"ai","lemma":"avoir","pos":"verb"}), Word({"surface":"pris","lemma":"prendre","pos":"verb"}), Word({"surface":"une","lemma":"une","pos":"article"}), Word({"surface":"douche","lemma":"douche","pos":"noun"})]

def SentenceWordlist2():
    return [Word({"surface":"He","lemma":"he","pos":"pronoun"}) , Word({"surface":"has","lemma":"have","pos":"verb"}), Word({"surface":"to","lemma":"to","pos":"adverb"}), Word({"surface":"switch","lemma":"switch","pos":"verb"}), Word({"surface":"the","lemma":"the","pos":"article"}), Word({"surface":"light","lemma":"light","pos":"noun"}),Word({"surface":"on","lemma":"on","pos":"adverb"})]

def SentenceWordlist3():
    return [Word({"surface":"I","lemma":"I","pos":"pronoun"}) , Word({"surface":"have","lemma":"have","pos":"verb"}), Word({"surface":"to","lemma":"to","pos":"adverb"}), Word({"surface":"switch","lemma":"switch","pos":"verb"}), Word({"surface":"the","lemma":"the","pos":"article"}), Word({"surface":"light","lemma":"light","pos":"noun"}),Word({"surface":"on","lemma":"on","pos":"adverb"})]

def CandidateWordlist1():
    return [Word({"lemma":"prendre"}),  Word({"lemma":"douche"})]
def CandidateWordlist2():
    return [Word({"lemma":"switch"}),  Word({"lemma":"on"})]


def test_make():
    #normal use
    cf = CandidateFactory()
    s1 = cf.make(word_list=CandidateWordlist2())
    assert isinstance(s1, Candidate) == True
    assert s1.id_number == 1
    assert len(s1.word_list) == 2

def test_uniquified():
    #normal use
    c1 = Candidate(3)
    c1.wordlist = CandidateWordlist1()
    cf = CandidateFactory()
    c2 = cf.uniquified(c1)
    assert c1.wordlist == c2.wordlist
    #print(cf.mapping)
    #assert len(cf.mapping) == 1
    

#def test___init__()

def test_add_occur_first_time():
    c1 = Candidate(1, word_list=CandidateWordlist1())
    c1.add_occur(Ngram(word_list=[Word({"surface":"J'","lemma":"je","pos":"pronoun"}), Word({"surface":"ai","lemma":"avoir","pos":"verb"})], freqs=None, sources=[1]))
    assert len(c1._dict_occurs) == 1
    for element in c1._dict_occurs:
        assert element.word_list[0]._props["lemma"] == "je"

def test_add_occur_second_time():
    c1 = Candidate(1, word_list=CandidateWordlist1())
    c1.add_occur(Ngram(word_list=[Word({"surface":"J'","lemma":"je","pos":"pronoun"}), Word({"surface":"ai","lemma":"avoir","pos":"verb"})], freqs=None, sources=[1]))
    c1.add_occur(Ngram(word_list=[Word({"surface":"J'","lemma":"je","pos":"pronoun"}), Word({"surface":"ai","lemma":"avoir","pos":"verb"})], freqs=None, sources=[3]))
    assert len(c1._dict_occurs) == 1
    for element in c1._dict_occurs:
        assert c1._dict_occurs[element].sources == [1,3]
    
    
#def test()
