#! /usr/bin/env python3
# -*- coding:UTF-8 -*-

def decent_str_split(string, separator=None):
    if not string:
        return []
    return string.split(separator)

def test_decent_str_split():
    assert decent_str_split("", "separator") == [] 
